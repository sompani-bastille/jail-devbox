#!/usr/local/bin/bash
set -euxo pipefail
service postgresql restart
rsync -PrhL --port=6969 sompani-db:/tmp/db/latest.sql.gz /tmp
cd /tmp
rm -rf latest.sql
gzip -d latest.sql.gz
su -m postgres -c 'psql -c "DROP DATABASE IF EXISTS "\""sompani-dev"\"'
su -m postgres -c 'psql -c "CREATE DATABASE "\""sompani-dev"\"" WITH OWNER = sompani_dev"'
perl -i -pe 's/sompani-prod/sompani_dev/g' ./latest.sql
perl -i -pe 's/sompani-staging/sompani_dev/g' ./latest.sql
perl -i -pe "/SELECT pg_catalog.set_config('search_path', '', false);/d" ./latest.sql
perl -i -pe "s/CREATE SCHEMA extensions;//g" ./latest.sql

psql --dbname=sompani-dev --username=sompani_dev \
  -c 'BEGIN TRANSACTION;' \
  -c 'CREATE SCHEMA extensions;' \
  -c 'CREATE EXTENSION IF NOT EXISTS timescaledb WITH SCHEMA extensions;' \
  -c 'SELECT timescaledb_pre_restore();' \
  -f ./latest.sql \
  -c 'CREATE EXTENSION IF NOT EXISTS timescaledb WITH SCHEMA extensions;' \
  -c 'COMMIT;' \
  --echo-errors \
  -v ON_ERROR_STOP=1
psql --dbname=sompani-dev --username=sompani_dev -c 'SELECT timescaledb_post_restore();'
cd -
psql -d sompani-dev -U sompani_dev --echo-errors --single-transaction -v ON_ERROR_STOP=1 -f ./reset-password.sql
