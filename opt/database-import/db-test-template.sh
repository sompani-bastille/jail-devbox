#!/usr/local/bin/bash
# Script to generate a database template
set -euxo pipefail
ssh zilti@db.sompani.com -t "sudo bastille cmd demo-db pg_dump --format=plain --no-owner --dbname=sompani-demo --file=/tmp/testdb.sql"
ssh zilti@db.sompani.com -t "sudo cat /usr/local/bastille/jails/demo-db/root/tmp/testdb.sql" > testdb.sql
perl -i -pe 's/sompani-prod/sompani_dev/g' ./testdb.sql
perl -i -pe 's/sompani-demo/sompani_dev/g' ./testdb.sql
perl -i -pe "/SELECT pg_catalog.set_config('search_path', '', false);/d" ./testdb.sql
perl -i -pe "s/CREATE SCHEMA extensions;//g" ./testdb.sql
su -m postgres -c 'psql -c "DROP DATABASE IF EXISTS "\""sompani-dev"\"""'
su -m postgres -c 'psql -c "UPDATE pg_database SET datistemplate=false WHERE datname='\''sompani-tpl'\''"'
su -m postgres -c 'psql -c "DROP DATABASE IF EXISTS "\""sompani-tpl"\"""'
su -m postgres -c 'psql -c "DROP USER IF EXISTS "\""sompani_tpl"\"""'
su -m postgres -c 'psql -c "CREATE USER sompani_tpl WITH SUPERUSER PASSWORD '\''1234'\''"'
su -m postgres -c 'psql -c "CREATE DATABASE "\""sompani-tpl"\"" WITH OWNER = sompani_tpl IS_TEMPLATE = true"'
psql --dbname=sompani-tpl --username=sompani_tpl -f ./testdb.sql
su -m postgres -c 'psql -c "CREATE DATABASE "\""sompani-dev"\"" WITH OWNER = sompani_dev TEMPLATE = "\""sompani-tpl"\"'
